"use strict";

// Enable chromereload by uncommenting this line:
import 'chromereload/devonly';

import $ from 'jquery';
global.jQuery = require('jquery');

import Api from './api.js';
import * as background_api from './background_api.js';
import * as accountsRefreshPage from './accounts_refresh_page.js';

import {messagesFromContentScript, messagesFromBackgroundScript, notifications, pageTypes} from './constants.js';
import * as helper from './helper.js';


let pageType = pageTypes.NORMAL;
let pageUri = null;
let myNotificationID = null;

let api = new Api(accountsRefreshPage.onConnectionError());

chrome.runtime.onInstalled.addListener(function (details) {
  console.log('previousVersion', details.previousVersion);
});

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  if (request.from == 'contentscript.js') {
    console.log('Request has been received from contentscript.js.');

    if (request.message == messagesFromContentScript.NOTIFY_PAGESIGNIN) {
      chrome.browserAction.setBadgeText({"text": "key"});
      chrome.browserAction.setBadgeBackgroundColor({"color": "green"});

      pageUri = request.uri;
      pageType = pageTypes.SIGNIN;

      let notification = notifications.ASK_INPUTPASSWORD;
      notification.message = `Create new account for ${pageUriuri}?`
      chrome.notifications.create("", notification, function (id) {
        myNotificationID = id;
      });

    } else if (request.message == messagesFromContentScript.NOTIFY_PAGESIGNUP) {
      chrome.browserAction.setBadgeText({"text": "nokey"});
      chrome.browserAction.setBadgeBackgroundColor({"color": "red"});

      pageType = pageTypes.SIGNUP;
      pageUri = request.uri;

      background_api.isAccountExist(pageUri, function(accountExist) {
        if (accountExist) {
          let notification = notifications.ACCOUNT_EXISTS;
          notification.message = `Account for ${pageUri} exists.`

          chrome.notifications.create("", notification, function (id) {
            myNotificationID = id;
          });
        } else {
          let notification = notifications.ASK_CREATEACCOUNT;
          notification.message = `Create new account for ${pageUri}?`

          chrome.notifications.create("", notification, function (id) {
            myNotificationID = id;
          });
        }
      });

    } else if (request.message == messagesFromContentScript.NOTIFY_PAGENORMAL) {
      chrome.browserAction.setBadgeText({"text": ""});
      chrome.browserAction.setBadgeBackgroundColor({"color": "black"});

      let typePage = pageTypes.NORMAL;
    } else if (request.message == messagesFromContentScript.NOTIFY_COMMAND_CREATEACCOUNT) {
      let args = request.args;
      background_api.createAccount(args.name, args.user, args.uri, args.passwordType, args.counter);

      let notification = notifications.ACCOUNT_CREATED;
      notification.message = `Account created for ${args.uri}.`
      chrome.notifications.create("", notification, function (id) {
        myNotificationID = id;
      });
    };
  }

  if (request.from == 'popup.js') {
    console.log('Request has been received from popup.js.');
    if (request.message == messagesFromPopup.REQUEST_INPUTMASTERPASSWORD) {
      api.inputMasterPassword(funcOnInputMasterPassword);
    }
  }

});

/* Respond to the user's clicking one of the buttons */
chrome.notifications.onButtonClicked.addListener(function (notificationID, buttonIndex) {
  chrome.notifications.clear(myNotificationID);
  if (notificationID === myNotificationID) {
    if (buttonIndex === 0) {
      if (pageType === pageTypes.SIGNIN) {
        alert('Sign in!');
        
      } else if (pageType === pageTypes.SIGNUP) {
        let uri = helper.extractDomainFromUri(pageUri);
        chrome.tabs.create({url:"../pages/accounts.html?createAccountUri=" + uri}, function(tab) { });
      }
    }
  }
});

let funcOnConnectionError = function onConnectionError() {
  $('#importModalEditAccount').load(chrome.extension.getURL('../pages/modal_connection_error.html'), function () {
    $('#connectionErrorModal').modal('show');
  });
};

let funcOnInputMasterPassword = function onInputMasterPassword(result) {
  console.log(result)
};

console.log('background.js loaded.');
