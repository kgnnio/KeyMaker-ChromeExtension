"use strict";

//const CONNECTION_ATTEMPTS_MAX = 32;

export default class NativeMessaging {
  constructor(callbackOnConnectionFailure) {
    this.connectionAttempt = 0;
    this.callback = null;
    this.callbackOnConnectionFailure = callbackOnConnectionFailure
    this.hostName = "io.gnn.keymaker"; //TODO Pull from manifest
    this.port = null;
  }

  connect() {
    console.log('NativeMessaging.js: Connecting to backend host: ' + this.hostName);
    this.port = chrome.runtime.connectNative(this.hostName);
    this.port.onMessage.addListener(this.onMessage.bind(this));
    this.port.onDisconnect.addListener(this.onDisconnected.bind(this));
  }

  onDisconnected() {
    console.log('NativeMessaging.js: Disconnected: ' + chrome.runtime.lastError.message);
    /*
    this.connectionAttempt += 1;

    if (this.connectionAttempt > CONNECTION_ATTEMPTS_MAX) {
      this.port = null;
      this.connectionAttempt = 0;
      this.funcCallbackOnFail();
    } else {
      console.log('NativeMessaging.js: Retrying. Attempt ' + this.connectionAttempt + ' out of ' + CONNECTION_ATTEMPTS_MAX);
      this.connect();
      this.postMessage(this.message, this.callback);
    }*/
  }

  postMessage(message, callback) {
    this.message = message;
    this.callback = callback;
    console.log('NativeMessaging.js: Posting message: ' + JSON.stringify(this.message));
    this.port.postMessage(message);
  }

  onMessage(message) {
    //TODO Check message type and whether we need all this conversion
    let strMessage = JSON.stringify(message);
    console.log('NativeMessaging.js: Received message: ' + strMessage);

    this.port = null;

    let objMessage = jQuery.parseJSON(strMessage);
    this.callback(objMessage);
  }
}
