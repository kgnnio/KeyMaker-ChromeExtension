"use strict";


export function extractDomainFromUri(uri) {
  let domain;

  if (uri.indexOf("://") > -1) {
    domain = uri.split('/')[2];
  } else {
    domain = uri.split('/')[0];
  }
  domain = domain.split(':')[0];

  return domain;
}

export function getUrlParameter(sParam) {
    let sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
