"use strict";

import $ from 'jquery';
global.jQuery = require('jquery');
require('bootstrap');
require('bootstrap-table');

import * as helper from './helper.js';

export let funcInitAccountTable = function initAccountTable(commandResult) {
  let accounts = commandResult['accounts'];

  accounts = presentifyAccountData(accounts);

  $('#tableAccounts').bootstrapTable({
    columns: [{
      checkbox: true
    }, {
      field: 'name',
      title: 'Name'
    }, {
      field: 'user',
      title: 'User'
    }, {
      field: 'uriShortened',
      title: 'URI'
    }, {
      field: 'passwordTypeName',
      title: 'Password Type'
    }, {
      field: 'counter',
      title: 'Counter'
    }, {
      field: 'action',
      title: '',
      formatter: actionFormatter
    }],
    data: accounts
  });

  $("#tableAccounts").removeClass('hidden');

  function actionFormatter(value, row, index) {
    return [
      '<div class="action-icons">',
      '<i class="buttonPassword glyphicon glyphicon-eye-open icon-password-color"></i>&nbsp;',
      '<i class="button_edit glyphicon glyphicon-edit icon-edit-color"></i>&nbsp;',
      '<i class="button_remove glyphicon glyphicon-trash icon-trash-color"></i>',
      '</div>'
    ].join('');
  }
};

export let funcRefreshAccountTable = function refreshAccountTable(result) {
  let accounts = result['accounts'];
  accounts = presentifyAccountData(accounts);
  $('#tableAccounts').bootstrapTable('load', {
    data: accounts
  });
};

export function onConnectionError() {
  $('#importModalEditAccount').load(chrome.extension.getURL('../pages/modal_connection_error.html'), function () {
    $('#connectionErrorModal').modal('show');
  });
};

function presentifyAccountData(accounts) {
  let passwordTypeNames = ['Maximum Security', 'Long', 'Medium', 'Short', 'Basic', 'PIN'];
  for (let account of accounts) {
    account.uriShortened = '<a href=\"' + account.uri + '\">' + helper.extractDomainFromUri(account.uri) + '</a>'
    account.passwordTypeName = passwordTypeNames[account.passwordType];
  }
  return accounts;
}
