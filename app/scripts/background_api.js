"use strict";

import Api from './api.js';
import * as backgroundInteractPage from './background_interact_page.js';
import {notifications} from './constants.js';

let api = new Api(funcDoNothing);

export function createAccount(name, user, uri, passwordType, counter) {
  api.accountCreate(name, user, uri, passwordType, counter, funcDoNothing);
}

export function isAccountExist(account_uri, callback) {
  api.isAccountExist(account_uri, function(message) {
    if (message.success) {
      callback(message.exists);
    };
  });
};

let funcDoNothing = function doNothing(message) {
  if (message.success === true) {
    
  } else {
    console.log(message.errorMessage);
  }
};
