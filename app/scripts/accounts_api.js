"use strict";

import Api from './api.js';
import * as accountsRefreshPage from './accounts_refresh_page.js';

let api = new Api(accountsRefreshPage.onConnectionError());

export function init() {
  api.getAccounts(accountsRefreshPage.funcInitAccountTable);
}

export function createAccount(name, user, uri, passwordType, counter) {
  api.accountCreate(name, user, uri, passwordType, counter, funcRefreshTable);
}

export function updateAccount(id, name, user, uri, passwordType, counter) {
  api.updateAccount(id, name, user, uri, passwordType, counter, funcRefreshTable);
}

export function deleteAccount(id) {
  api.deleteAccount(id, funcRefreshTable);
}

export function deleteAccounts(ids) {
  api.deleteAccounts(ids, funcRefreshTable);
}

export function requestPassword(id) {
  api.requestPassword(id, checkResult);
}

let funcRefreshTable = function refreshTable(message) {
  if (message.success === true) {
    api.getAccounts(accountsRefreshPage.funcRefreshAccountTable);
  } else {
    console.log(message.errorMessage);
  }
};

let checkResult = function checkResult(message) {
  if (message.success === true) {
    console.log(message);
    alert(message);
  } else {
    console.log(message.errorMessage);
  }
};
