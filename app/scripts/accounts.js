"use strict";

import $ from 'jquery';
global.jQuery = require('jquery');
require('bootstrap');
require('bootstrap-table');

import * as accounts_api from './accounts_api.js';
import * as helper from './helper.js';
import {messagesFromBackgroundScript} from './constants.js';

$(document).delegate("#buttonCreateAccount", "click", function () {
  $('#importModalEditAccount').load(chrome.extension.getURL('../pages/modal_edit_account.html'), function () {
    $('#editAccountModalTitle').text('Create Account');

    $('#editAccountModalInputName').val = '';
    $('#editAccountModalInputUser').val = '';
    $('#editAccountModalInputUri').val = '';
    $('#editAccountModalInputUpdates').val = '1';

    $('#editAccountModal').modal('show');
  });
});

$(document).delegate("#editAccountModalButtonSave", "click", function () {
  let id = document.getElementById('editAccountModalId').value;
  let name = document.getElementById('editAccountModalInputName').value;
  let user = document.getElementById('editAccountModalInputUser').value;
  let uri = document.getElementById('editAccountModalInputUri').value;
  let passwordType = document.getElementById('editAccountModalSelectPasswordType').selectedIndex;
  let counter = document.getElementById('editAccountModalInputCounter').value;

  $('#editAccountModal').modal('hide');

  if (id === "") {
    accounts_api.createAccount(name, user, uri, passwordType, counter);
  } else {
    accounts_api.updateAccount(id, name, user, uri, passwordType, counter);
  }
});

$(document).delegate("#buttonDeleteAccounts", "click", function () {
  let accounts = $('#tableAccounts').bootstrapTable('getSelections');
  console.log(accounts);
  console.log(typeof(accounts));
  if (accounts.length === 0) {
    return false;
  }

  $('#importModalDeleteAccount').load(chrome.extension.getURL('../pages/modal_delete_account.html'), function () {
    for (let account of accounts) {
      $('#deleteAccountModalAccounts').append('<li>' + account.name + '</li>');
    }
    $('#deleteAccountModal').modal('show');

    $(document).delegate("#deleteAccountModalButtonDelete", "click", function () {
      $('#deleteAccountModal').modal('hide');

      let accounts = $('#tableAccounts').bootstrapTable('getSelections');
      let accounts_to_delete_ids = [];
      for (let account of accounts) {
        accounts_to_delete_ids.push(account.id);
      }
      accounts_api.deleteAccounts(accounts_to_delete_ids);
    });
  });
});

window.actionEvents = {
  'click .buttonPassword': function (e, value, row, index) {
    let account = row;
    accounts_api.requestPassword(account['id']);
  },
  'click .button_edit': function (e, value, row, index) {
    $('#importModalEditAccount').load(chrome.extension.getURL('../pages/modal_edit_account.html'), function () {
      $('#editAccountModalTitle').text('Edit Account');

      let account = row;
      $('#editAccountModalId').val(account['id']);
      $('#editAccountModalInputName').val(account['name']);
      $('#editAccountModalInputUser').val(account['user']);
      $('#editAccountModalInputUri').val(account['uri']);
      $('#editAccountModalSelectPasswordType').val(account['passwordType']);
      $('#editAccountModalInputCounter').val(account['counter']);

      $('#editAccountModal').modal('show');
    });
  },
  'click .button_remove': function (e, value, row, index) {
    $('#importModalDeleteAccount').load(chrome.extension.getURL('../pages/modal_delete_account.html'), function () {
      let account = row;
      $('#deleteAccountModalAccounts').append('<li>' + account.name + '</li>');

      $('#deleteAccountModal').modal('show');

      $(document).delegate("#deleteAccountModalButtonDelete", "click", function () {
        $('#deleteAccountModal').modal('hide');

        accounts_api.deleteAccount(account.id);
      });
    });
  }
};

function createAccount() {
  $('#importModalEditAccount').load(chrome.extension.getURL('../pages/modal_edit_account.html'), function () {
    $('#editAccountModalTitle').text('KeyMaker - Create Account');

    let id = ''
    let name = helper.extractDomainFromUri(window.location.href);
    let user = '' // TODO Implement default user
    let uri = helper.extractDomainFromUri(window.location.href);
    let passwordType = 1;
    let counter = 1;

    if(helper.getUrlParameter('createAccountUri') != undefined){
      name = helper.getUrlParameter('createAccountUri');
      uri = helper.getUrlParameter('createAccountUri');
      window.history.pushState('accounts.html', '', '/pages/accounts.html');
    }

    $('editAccountModalId').val(id);
    $('#editAccountModalInputName').val(name);
    $('#editAccountModalInputUser').val(user);
    $('#editAccountModalInputUri').val(uri);
    $('#editAccountModalSelectPasswordType').val(passwordType);
    $('#editAccountModalInputCounter').val(counter);

    $('#editAccountModal').modal('show');
  });

  $(document).delegate("#editAccountModalButtonSave", "click", function () {
    let id = document.getElementById('editAccountModalId').value;
    let name = document.getElementById('editAccountModalInputName').value;
    let user = document.getElementById('editAccountModalInputUser').value;
    let uri = document.getElementById('editAccountModalInputUri').value;
    let passwordType = document.getElementById('editAccountModalSelectPasswordType').selectedIndex;
    let counter = document.getElementById('editAccountModalInputCounter').value;

    let args = {id: id, name: name, user: user, uri: uri, passwordType: passwordType, counter: counter}

    $('#editAccountModal').modal('hide');

    chrome.runtime.sendMessage({from: 'new_account.js', message: messagesFromContentScript.NOTIFY_COMMAND_CREATEACCOUNT, args: args});
  });
}

$(document).ready(function () {
  accounts_api.init();
  if(helper.getUrlParameter('createAccountUri') != undefined){
    createAccount();
  }
});
