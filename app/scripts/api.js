"use strict";

import NativeMessaging from './native_messaging.js';

export default class Api {
  constructor(onFail) {
    this.nativeMessaging = new NativeMessaging(onFail);
  }

  accountCreate(name, user, uri, passwordType, counter, callback) {
    this.nativeMessaging.connect();
    this.nativeMessaging.postMessage({
      "CreateAccount": [
        {
          "name": name,
          "user": user,
          "uri": uri,
          "passwordType": Number(passwordType),
          "counter": Number(counter)
        }
      ]
    }, callback);
  }

  updateAccount(id, name, user, uri, passwordType, counter, callback) {
    this.nativeMessaging.connect();
    this.nativeMessaging.postMessage({
      "UpdateAccount": [
        {
          "id": Number(id),
          "name": name,
          "user": user,
          "uri": uri,
          "passwordType": Number(passwordType),
          "counter": Number(counter)
        }
      ]
    }, callback);
  }

  deleteAccount(id, callback) {
    this.nativeMessaging.connect();
    this.nativeMessaging.postMessage({
      "DeleteAccount": [
        {
          "id": Number(id)
        }
      ]
    }, callback);
  }

  deleteAccounts(ids, callback) {
    this.nativeMessaging.connect();
    this.nativeMessaging.postMessage({
      "DeleteAccounts": [
        {
          "ids": ids
        }
      ]
    }, callback);
  }

  getAccounts(callback) {
    this.nativeMessaging.connect();
    this.nativeMessaging.postMessage({
      "GetAccounts": []
    }, callback);
  }

  isAccountExist(uri, callback) {
    this.nativeMessaging.connect();
    this.nativeMessaging.postMessage({
      "IsAccountExist": [
        {
          "uri": uri
        }
      ]
    }, callback);
  }

  requestPassword(id, callback) {
    this.nativeMessaging.connect();
    this.nativeMessaging.postMessage({
      "RequestPassword": [
        {
          "id": id
        }
      ]
    }, callback);
  }

  inputMasterPassword(callback) {
    this.nativeMessaging.connect();
    this.nativeMessaging.postMessage({
      "InputMasterPassword": []
    }, callback);
  }
}
