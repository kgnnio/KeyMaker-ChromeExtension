"use strict";

import $ from 'jquery';
global.jQuery = require('jquery');
require('bootstrap');
require('bootstrap-table');

import {notifications} from './constants.js';


export function notifyAccountNotExists(newAccountUri) {
    let notification = notifications.ASK_CREATEACCOUNT;
    notification.message = `Create new account for ${newAccountUri}?`

    let myNotificationID = null;
    chrome.notifications.create("", notification, function (id) {
        console.log('id: ' + id);
        myNotificationID = id;
        console.log('myNotificationID1 ' + myNotificationID);
        
    });

};


export function notifyAccountExists(newAccountUri) {
    let notification = notifications.ACCOUNT_EXISTS;
    notification.message = `Account for ${newAccountUri} exists.`
    
    let myNotificationID = null;
    chrome.notifications.create("", notification, function (id) {
        console.log('id: ' + id);
        myNotificationID = id;
        console.log('myNotificationID1 ' + myNotificationID);
    });
    console.log('myNotificationID2 ' + myNotificationID);
    return myNotificationID;
};
