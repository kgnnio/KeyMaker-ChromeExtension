"use strict";

import $ from 'jquery';
global.jQuery = require('jquery');
require('bootstrap');

import * as accounts_api from './accounts_api.js';
import {messagesFromContentScript, messagesFromBackgroundScript} from './constants.js';
import * as helper from './helper.js';

let numOfInputPasswordOnPage = $("input:password").length;
let uri = helper.extractDomainFromUri(window.location.href);

function determinePageType() {
  if (numOfInputPasswordOnPage === 1) {
    chrome.runtime.sendMessage({from: 'contentscript.js', message: messagesFromContentScript.NOTIFY_PAGESIGNIN, uri: uri});

  } else if (numOfInputPasswordOnPage === 2) {
    chrome.runtime.sendMessage({from: 'contentscript.js', message: messagesFromContentScript.NOTIFY_PAGESIGNUP, uri: uri});

  } else {
    chrome.runtime.sendMessage({from: 'contentscript.js', message: messagesFromContentScript.NOTIFY_PAGENORMAL});
  }
}


$(document).ready(function () {
  console.log('contentscript.js loaded.');
  $(document.body).append('<div id="importModalEditAccount"></div>');
  determinePageType();
});