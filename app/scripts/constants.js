"use strict";

import * as accounts_api from './accounts_api.js';

export const messagesFromContentScript = {
  NOTIFY_COMMAND_CREATEACCOUNT: 'notifyCommandCreateAccount',
  NOTIFY_PAGESIGNIN: 'notifyPageSignin',
  NOTIFY_PAGESIGNUP: 'notifyPageSignup',
  NOTIFY_PAGENORMAL: 'notifyPageNormal'
};

export const messagesFromBackgroundScript = {
    NOTIFY_COMMAND_CREATEACCOUNT_BUTTON_YES_CLICKED: 'notifyCommandCreateAccountButtonYesClicked',
    NOTIFY_CREATEACCOUNT: 'notifyCommandCreateAccount'
};

export const messagesFromPopup = {
  REQUEST_INPUTMASTERPASSWORD: 'requestInputMasterPassword'
};

export const notifications = {
  ASK_INPUTPASSWORD: {
    type: "basic",
    iconUrl: "/images/icon-128.png",
    title: "KeyMaker",
    message: "Input password for %s?",
    contextMessage: "KeyMaker",
    buttons: [{
      title: "Yes",
      iconUrl: "/images/icon-19.png"
    }]
  },
  ASK_CREATEACCOUNT: {
    type: "basic",
    iconUrl: "/images/icon-128.png",
    title: "KeyMaker",
    message: "Create new account for %s?",
    buttons: [{
      title: "Yes",
      iconUrl: "/images/icon-19.png"
    }]
  },
  ACCOUNT_CREATED: {
    type: "basic",
    iconUrl: "/images/icon-128.png",
    title: "KeyMaker",
    message: "Account created for %s.",
  },
  ACCOUNT_EXISTS: {
    type: "basic",
    iconUrl: "/images/icon-128.png",
    title: "KeyMaker",
    message: "Account for %s exists.",
  }
}

export const pageTypes = {
  NORMAL: 0,
  SIGNIN: 1,
  SIGNUP: 2
}
