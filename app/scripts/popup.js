"use strict";

import $ from 'jquery';
global.jQuery = require('jquery');
require('bootstrap');
require('bootstrap-table');
require('bootstrap-toggle');

import {messages} from './constants.js';

var background = chrome.extension.getBackgroundPage();

document.addEventListener('DOMContentLoaded', function() {
  var checkPageButton = document.getElementById('buttonManageMyAccounts');
  checkPageButton.addEventListener('click', function() {
    chrome.tabs.create({ url: "../pages/accounts.html" });
  }, false);
}, false);

function getPageState() {
  var a = 0;
  chrome.runtime.sendMessage({from: 'popup.js', message:"getPageType"},function(response){
    a = response.response;
    console.log(a);
  });
  return a;
}

$(function() {
  $('#toggleIsMasterPasswordRegistered').change(function() {
    //$('#toggleIsMasterPasswordRegistered').bootstrapToggle('disable')
    chrome.runtime.sendMessage({from: 'popup.js', message:messagesFromPopup.REQUEST_INPUTMASTERPASSWORD},function(response){
      //$('#toggleIsMasterPasswordRegistered').bootstrapToggle('enable')
      //$('#toggleIsMasterPasswordRegistered').bootstrapToggle('on');
    });
  })
})

console.log('pageState: ' + getPageState());
console.log('popup.js loaded.');

$(document).ready(function () {
  $('#toggleIsMasterPasswordRegistered').bootstrapToggle('off');
});
