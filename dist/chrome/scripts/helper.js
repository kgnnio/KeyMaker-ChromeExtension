/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(7);


/***/ },

/***/ 7:
/***/ function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	exports.extractDomainFromUri = extractDomainFromUri;
	exports.getUrlParameter = getUrlParameter;
	function extractDomainFromUri(uri) {
	    var domain = void 0;
	
	    if (uri.indexOf("://") > -1) {
	        domain = uri.split('/')[2];
	    } else {
	        domain = uri.split('/')[0];
	    }
	    domain = domain.split(':')[0];
	
	    return domain;
	}
	
	function getUrlParameter(sParam) {
	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName = void 0,
	        i = void 0;
	
	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');
	
	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }
	    }
	};

/***/ }

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYmZiNGM0ZWZjMWU4ZTA2MGE5YmE/ZjljNioqKioqKioqIiwid2VicGFjazovLy8uL2FwcC9zY3JpcHRzL2hlbHBlci5qcz8zM2Y4KioqKioqKiJdLCJuYW1lcyI6WyJleHRyYWN0RG9tYWluRnJvbVVyaSIsImdldFVybFBhcmFtZXRlciIsInVyaSIsImRvbWFpbiIsImluZGV4T2YiLCJzcGxpdCIsInNQYXJhbSIsInNQYWdlVVJMIiwiZGVjb2RlVVJJQ29tcG9uZW50Iiwid2luZG93IiwibG9jYXRpb24iLCJzZWFyY2giLCJzdWJzdHJpbmciLCJzVVJMVmFyaWFibGVzIiwic1BhcmFtZXRlck5hbWUiLCJpIiwibGVuZ3RoIiwidW5kZWZpbmVkIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsdUJBQWU7QUFDZjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7OztBQ3RDQTs7Ozs7U0FHZ0JBLG9CLEdBQUFBLG9CO1NBYUFDLGUsR0FBQUEsZTtBQWJULFVBQVNELG9CQUFULENBQThCRSxHQUE5QixFQUFtQztBQUN4QyxTQUFJQyxlQUFKOztBQUVBLFNBQUlELElBQUlFLE9BQUosQ0FBWSxLQUFaLElBQXFCLENBQUMsQ0FBMUIsRUFBNkI7QUFDM0JELGtCQUFTRCxJQUFJRyxLQUFKLENBQVUsR0FBVixFQUFlLENBQWYsQ0FBVDtBQUNELE1BRkQsTUFFTztBQUNMRixrQkFBU0QsSUFBSUcsS0FBSixDQUFVLEdBQVYsRUFBZSxDQUFmLENBQVQ7QUFDRDtBQUNERixjQUFTQSxPQUFPRSxLQUFQLENBQWEsR0FBYixFQUFrQixDQUFsQixDQUFUOztBQUVBLFlBQU9GLE1BQVA7QUFDRDs7QUFFTSxVQUFTRixlQUFULENBQXlCSyxNQUF6QixFQUFpQztBQUNwQyxTQUFJQyxXQUFXQyxtQkFBbUJDLE9BQU9DLFFBQVAsQ0FBZ0JDLE1BQWhCLENBQXVCQyxTQUF2QixDQUFpQyxDQUFqQyxDQUFuQixDQUFmO0FBQUEsU0FDSUMsZ0JBQWdCTixTQUFTRixLQUFULENBQWUsR0FBZixDQURwQjtBQUFBLFNBRUlTLHVCQUZKO0FBQUEsU0FHSUMsVUFISjs7QUFLQSxVQUFLQSxJQUFJLENBQVQsRUFBWUEsSUFBSUYsY0FBY0csTUFBOUIsRUFBc0NELEdBQXRDLEVBQTJDO0FBQ3ZDRCwwQkFBaUJELGNBQWNFLENBQWQsRUFBaUJWLEtBQWpCLENBQXVCLEdBQXZCLENBQWpCOztBQUVBLGFBQUlTLGVBQWUsQ0FBZixNQUFzQlIsTUFBMUIsRUFBa0M7QUFDOUIsb0JBQU9RLGVBQWUsQ0FBZixNQUFzQkcsU0FBdEIsR0FBa0MsSUFBbEMsR0FBeUNILGVBQWUsQ0FBZixDQUFoRDtBQUNIO0FBQ0o7QUFDSixHIiwiZmlsZSI6ImhlbHBlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKVxuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuXG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRleHBvcnRzOiB7fSxcbiBcdFx0XHRpZDogbW9kdWxlSWQsXG4gXHRcdFx0bG9hZGVkOiBmYWxzZVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sb2FkZWQgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKDApO1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogd2VicGFjay9ib290c3RyYXAgYmZiNGM0ZWZjMWU4ZTA2MGE5YmFcbiAqKi8iLCJcInVzZSBzdHJpY3RcIjtcblxuXG5leHBvcnQgZnVuY3Rpb24gZXh0cmFjdERvbWFpbkZyb21VcmkodXJpKSB7XG4gIGxldCBkb21haW47XG5cbiAgaWYgKHVyaS5pbmRleE9mKFwiOi8vXCIpID4gLTEpIHtcbiAgICBkb21haW4gPSB1cmkuc3BsaXQoJy8nKVsyXTtcbiAgfSBlbHNlIHtcbiAgICBkb21haW4gPSB1cmkuc3BsaXQoJy8nKVswXTtcbiAgfVxuICBkb21haW4gPSBkb21haW4uc3BsaXQoJzonKVswXTtcblxuICByZXR1cm4gZG9tYWluO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0VXJsUGFyYW1ldGVyKHNQYXJhbSkge1xuICAgIGxldCBzUGFnZVVSTCA9IGRlY29kZVVSSUNvbXBvbmVudCh3aW5kb3cubG9jYXRpb24uc2VhcmNoLnN1YnN0cmluZygxKSksXG4gICAgICAgIHNVUkxWYXJpYWJsZXMgPSBzUGFnZVVSTC5zcGxpdCgnJicpLFxuICAgICAgICBzUGFyYW1ldGVyTmFtZSxcbiAgICAgICAgaTtcblxuICAgIGZvciAoaSA9IDA7IGkgPCBzVVJMVmFyaWFibGVzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIHNQYXJhbWV0ZXJOYW1lID0gc1VSTFZhcmlhYmxlc1tpXS5zcGxpdCgnPScpO1xuXG4gICAgICAgIGlmIChzUGFyYW1ldGVyTmFtZVswXSA9PT0gc1BhcmFtKSB7XG4gICAgICAgICAgICByZXR1cm4gc1BhcmFtZXRlck5hbWVbMV0gPT09IHVuZGVmaW5lZCA/IHRydWUgOiBzUGFyYW1ldGVyTmFtZVsxXTtcbiAgICAgICAgfVxuICAgIH1cbn07XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2FwcC9zY3JpcHRzL2hlbHBlci5qc1xuICoqLyJdLCJzb3VyY2VSb290IjoiIn0=