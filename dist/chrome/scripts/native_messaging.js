/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(5);


/***/ },

/***/ 5:
/***/ function(module, exports) {

	"use strict";
	
	//const CONNECTION_ATTEMPTS_MAX = 32;
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var NativeMessaging = function () {
	  function NativeMessaging(callbackOnConnectionFailure) {
	    _classCallCheck(this, NativeMessaging);
	
	    this.connectionAttempt = 0;
	    this.callback = null;
	    this.callbackOnConnectionFailure = callbackOnConnectionFailure;
	    this.hostName = "io.gnn.keymaker"; //TODO Pull from manifest
	    this.port = null;
	  }
	
	  _createClass(NativeMessaging, [{
	    key: "connect",
	    value: function connect() {
	      console.log('NativeMessaging.js: Connecting to backend host: ' + this.hostName);
	      this.port = chrome.runtime.connectNative(this.hostName);
	      this.port.onMessage.addListener(this.onMessage.bind(this));
	      this.port.onDisconnect.addListener(this.onDisconnected.bind(this));
	    }
	  }, {
	    key: "onDisconnected",
	    value: function onDisconnected() {
	      console.log('NativeMessaging.js: Disconnected: ' + chrome.runtime.lastError.message);
	      /*
	      this.connectionAttempt += 1;
	       if (this.connectionAttempt > CONNECTION_ATTEMPTS_MAX) {
	        this.port = null;
	        this.connectionAttempt = 0;
	        this.funcCallbackOnFail();
	      } else {
	        console.log('NativeMessaging.js: Retrying. Attempt ' + this.connectionAttempt + ' out of ' + CONNECTION_ATTEMPTS_MAX);
	        this.connect();
	        this.postMessage(this.message, this.callback);
	      }*/
	    }
	  }, {
	    key: "postMessage",
	    value: function postMessage(message, callback) {
	      this.message = message;
	      this.callback = callback;
	      console.log('NativeMessaging.js: Posting message: ' + JSON.stringify(this.message));
	      this.port.postMessage(message);
	    }
	  }, {
	    key: "onMessage",
	    value: function onMessage(message) {
	      //TODO Check message type and whether we need all this conversion
	      var strMessage = JSON.stringify(message);
	      console.log('NativeMessaging.js: Received message: ' + strMessage);
	
	      this.port = null;
	
	      var objMessage = jQuery.parseJSON(strMessage);
	      this.callback(objMessage);
	    }
	  }]);
	
	  return NativeMessaging;
	}();
	
	exports.default = NativeMessaging;

/***/ }

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgYmZiNGM0ZWZjMWU4ZTA2MGE5YmE/ZjljNioqKioqKioqKiIsIndlYnBhY2s6Ly8vLi9hcHAvc2NyaXB0cy9uYXRpdmVfbWVzc2FnaW5nLmpzPzQ4YWMqKioqKioqIl0sIm5hbWVzIjpbIk5hdGl2ZU1lc3NhZ2luZyIsImNhbGxiYWNrT25Db25uZWN0aW9uRmFpbHVyZSIsImNvbm5lY3Rpb25BdHRlbXB0IiwiY2FsbGJhY2siLCJob3N0TmFtZSIsInBvcnQiLCJjb25zb2xlIiwibG9nIiwiY2hyb21lIiwicnVudGltZSIsImNvbm5lY3ROYXRpdmUiLCJvbk1lc3NhZ2UiLCJhZGRMaXN0ZW5lciIsImJpbmQiLCJvbkRpc2Nvbm5lY3QiLCJvbkRpc2Nvbm5lY3RlZCIsImxhc3RFcnJvciIsIm1lc3NhZ2UiLCJKU09OIiwic3RyaW5naWZ5IiwicG9zdE1lc3NhZ2UiLCJzdHJNZXNzYWdlIiwib2JqTWVzc2FnZSIsImpRdWVyeSIsInBhcnNlSlNPTiJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLHVCQUFlO0FBQ2Y7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0Q0E7O0FBRUE7Ozs7Ozs7Ozs7S0FFcUJBLGU7QUFDbkIsNEJBQVlDLDJCQUFaLEVBQXlDO0FBQUE7O0FBQ3ZDLFVBQUtDLGlCQUFMLEdBQXlCLENBQXpCO0FBQ0EsVUFBS0MsUUFBTCxHQUFnQixJQUFoQjtBQUNBLFVBQUtGLDJCQUFMLEdBQW1DQSwyQkFBbkM7QUFDQSxVQUFLRyxRQUFMLEdBQWdCLGlCQUFoQixDQUp1QyxDQUlKO0FBQ25DLFVBQUtDLElBQUwsR0FBWSxJQUFaO0FBQ0Q7Ozs7K0JBRVM7QUFDUkMsZUFBUUMsR0FBUixDQUFZLHFEQUFxRCxLQUFLSCxRQUF0RTtBQUNBLFlBQUtDLElBQUwsR0FBWUcsT0FBT0MsT0FBUCxDQUFlQyxhQUFmLENBQTZCLEtBQUtOLFFBQWxDLENBQVo7QUFDQSxZQUFLQyxJQUFMLENBQVVNLFNBQVYsQ0FBb0JDLFdBQXBCLENBQWdDLEtBQUtELFNBQUwsQ0FBZUUsSUFBZixDQUFvQixJQUFwQixDQUFoQztBQUNBLFlBQUtSLElBQUwsQ0FBVVMsWUFBVixDQUF1QkYsV0FBdkIsQ0FBbUMsS0FBS0csY0FBTCxDQUFvQkYsSUFBcEIsQ0FBeUIsSUFBekIsQ0FBbkM7QUFDRDs7O3NDQUVnQjtBQUNmUCxlQUFRQyxHQUFSLENBQVksdUNBQXVDQyxPQUFPQyxPQUFQLENBQWVPLFNBQWYsQ0FBeUJDLE9BQTVFO0FBQ0E7Ozs7Ozs7Ozs7O0FBWUQ7OztpQ0FFV0EsTyxFQUFTZCxRLEVBQVU7QUFDN0IsWUFBS2MsT0FBTCxHQUFlQSxPQUFmO0FBQ0EsWUFBS2QsUUFBTCxHQUFnQkEsUUFBaEI7QUFDQUcsZUFBUUMsR0FBUixDQUFZLDBDQUEwQ1csS0FBS0MsU0FBTCxDQUFlLEtBQUtGLE9BQXBCLENBQXREO0FBQ0EsWUFBS1osSUFBTCxDQUFVZSxXQUFWLENBQXNCSCxPQUF0QjtBQUNEOzs7K0JBRVNBLE8sRUFBUztBQUNqQjtBQUNBLFdBQUlJLGFBQWFILEtBQUtDLFNBQUwsQ0FBZUYsT0FBZixDQUFqQjtBQUNBWCxlQUFRQyxHQUFSLENBQVksMkNBQTJDYyxVQUF2RDs7QUFFQSxZQUFLaEIsSUFBTCxHQUFZLElBQVo7O0FBRUEsV0FBSWlCLGFBQWFDLE9BQU9DLFNBQVAsQ0FBaUJILFVBQWpCLENBQWpCO0FBQ0EsWUFBS2xCLFFBQUwsQ0FBY21CLFVBQWQ7QUFDRDs7Ozs7O21CQWhEa0J0QixlIiwiZmlsZSI6Im5hdGl2ZV9tZXNzYWdpbmcuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSlcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcblxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0ZXhwb3J0czoge30sXG4gXHRcdFx0aWQ6IG1vZHVsZUlkLFxuIFx0XHRcdGxvYWRlZDogZmFsc2VcbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubG9hZGVkID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXygwKTtcblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIHdlYnBhY2svYm9vdHN0cmFwIGJmYjRjNGVmYzFlOGUwNjBhOWJhXG4gKiovIiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbi8vY29uc3QgQ09OTkVDVElPTl9BVFRFTVBUU19NQVggPSAzMjtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTmF0aXZlTWVzc2FnaW5nIHtcbiAgY29uc3RydWN0b3IoY2FsbGJhY2tPbkNvbm5lY3Rpb25GYWlsdXJlKSB7XG4gICAgdGhpcy5jb25uZWN0aW9uQXR0ZW1wdCA9IDA7XG4gICAgdGhpcy5jYWxsYmFjayA9IG51bGw7XG4gICAgdGhpcy5jYWxsYmFja09uQ29ubmVjdGlvbkZhaWx1cmUgPSBjYWxsYmFja09uQ29ubmVjdGlvbkZhaWx1cmVcbiAgICB0aGlzLmhvc3ROYW1lID0gXCJpby5nbm4ua2V5bWFrZXJcIjsgLy9UT0RPIFB1bGwgZnJvbSBtYW5pZmVzdFxuICAgIHRoaXMucG9ydCA9IG51bGw7XG4gIH1cblxuICBjb25uZWN0KCkge1xuICAgIGNvbnNvbGUubG9nKCdOYXRpdmVNZXNzYWdpbmcuanM6IENvbm5lY3RpbmcgdG8gYmFja2VuZCBob3N0OiAnICsgdGhpcy5ob3N0TmFtZSk7XG4gICAgdGhpcy5wb3J0ID0gY2hyb21lLnJ1bnRpbWUuY29ubmVjdE5hdGl2ZSh0aGlzLmhvc3ROYW1lKTtcbiAgICB0aGlzLnBvcnQub25NZXNzYWdlLmFkZExpc3RlbmVyKHRoaXMub25NZXNzYWdlLmJpbmQodGhpcykpO1xuICAgIHRoaXMucG9ydC5vbkRpc2Nvbm5lY3QuYWRkTGlzdGVuZXIodGhpcy5vbkRpc2Nvbm5lY3RlZC5iaW5kKHRoaXMpKTtcbiAgfVxuXG4gIG9uRGlzY29ubmVjdGVkKCkge1xuICAgIGNvbnNvbGUubG9nKCdOYXRpdmVNZXNzYWdpbmcuanM6IERpc2Nvbm5lY3RlZDogJyArIGNocm9tZS5ydW50aW1lLmxhc3RFcnJvci5tZXNzYWdlKTtcbiAgICAvKlxuICAgIHRoaXMuY29ubmVjdGlvbkF0dGVtcHQgKz0gMTtcblxuICAgIGlmICh0aGlzLmNvbm5lY3Rpb25BdHRlbXB0ID4gQ09OTkVDVElPTl9BVFRFTVBUU19NQVgpIHtcbiAgICAgIHRoaXMucG9ydCA9IG51bGw7XG4gICAgICB0aGlzLmNvbm5lY3Rpb25BdHRlbXB0ID0gMDtcbiAgICAgIHRoaXMuZnVuY0NhbGxiYWNrT25GYWlsKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnNvbGUubG9nKCdOYXRpdmVNZXNzYWdpbmcuanM6IFJldHJ5aW5nLiBBdHRlbXB0ICcgKyB0aGlzLmNvbm5lY3Rpb25BdHRlbXB0ICsgJyBvdXQgb2YgJyArIENPTk5FQ1RJT05fQVRURU1QVFNfTUFYKTtcbiAgICAgIHRoaXMuY29ubmVjdCgpO1xuICAgICAgdGhpcy5wb3N0TWVzc2FnZSh0aGlzLm1lc3NhZ2UsIHRoaXMuY2FsbGJhY2spO1xuICAgIH0qL1xuICB9XG5cbiAgcG9zdE1lc3NhZ2UobWVzc2FnZSwgY2FsbGJhY2spIHtcbiAgICB0aGlzLm1lc3NhZ2UgPSBtZXNzYWdlO1xuICAgIHRoaXMuY2FsbGJhY2sgPSBjYWxsYmFjaztcbiAgICBjb25zb2xlLmxvZygnTmF0aXZlTWVzc2FnaW5nLmpzOiBQb3N0aW5nIG1lc3NhZ2U6ICcgKyBKU09OLnN0cmluZ2lmeSh0aGlzLm1lc3NhZ2UpKTtcbiAgICB0aGlzLnBvcnQucG9zdE1lc3NhZ2UobWVzc2FnZSk7XG4gIH1cblxuICBvbk1lc3NhZ2UobWVzc2FnZSkge1xuICAgIC8vVE9ETyBDaGVjayBtZXNzYWdlIHR5cGUgYW5kIHdoZXRoZXIgd2UgbmVlZCBhbGwgdGhpcyBjb252ZXJzaW9uXG4gICAgbGV0IHN0ck1lc3NhZ2UgPSBKU09OLnN0cmluZ2lmeShtZXNzYWdlKTtcbiAgICBjb25zb2xlLmxvZygnTmF0aXZlTWVzc2FnaW5nLmpzOiBSZWNlaXZlZCBtZXNzYWdlOiAnICsgc3RyTWVzc2FnZSk7XG5cbiAgICB0aGlzLnBvcnQgPSBudWxsO1xuXG4gICAgbGV0IG9iak1lc3NhZ2UgPSBqUXVlcnkucGFyc2VKU09OKHN0ck1lc3NhZ2UpO1xuICAgIHRoaXMuY2FsbGJhY2sob2JqTWVzc2FnZSk7XG4gIH1cbn1cblxuXG5cbi8qKiBXRUJQQUNLIEZPT1RFUiAqKlxuICoqIC4vYXBwL3NjcmlwdHMvbmF0aXZlX21lc3NhZ2luZy5qc1xuICoqLyJdLCJzb3VyY2VSb290IjoiIn0=