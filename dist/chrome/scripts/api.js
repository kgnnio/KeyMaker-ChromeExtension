/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(4);


/***/ },
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _native_messaging = __webpack_require__(5);
	
	var _native_messaging2 = _interopRequireDefault(_native_messaging);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var Api = function () {
	  function Api(onFail) {
	    _classCallCheck(this, Api);
	
	    this.nativeMessaging = new _native_messaging2.default(onFail);
	  }
	
	  _createClass(Api, [{
	    key: "accountCreate",
	    value: function accountCreate(name, user, uri, passwordType, counter, callback) {
	      this.nativeMessaging.connect();
	      this.nativeMessaging.postMessage({
	        "CreateAccount": [{
	          "name": name,
	          "user": user,
	          "uri": uri,
	          "passwordType": Number(passwordType),
	          "counter": Number(counter)
	        }]
	      }, callback);
	    }
	  }, {
	    key: "updateAccount",
	    value: function updateAccount(id, name, user, uri, passwordType, counter, callback) {
	      this.nativeMessaging.connect();
	      this.nativeMessaging.postMessage({
	        "UpdateAccount": [{
	          "id": Number(id),
	          "name": name,
	          "user": user,
	          "uri": uri,
	          "passwordType": Number(passwordType),
	          "counter": Number(counter)
	        }]
	      }, callback);
	    }
	  }, {
	    key: "deleteAccount",
	    value: function deleteAccount(id, callback) {
	      this.nativeMessaging.connect();
	      this.nativeMessaging.postMessage({
	        "DeleteAccount": [{
	          "id": Number(id)
	        }]
	      }, callback);
	    }
	  }, {
	    key: "deleteAccounts",
	    value: function deleteAccounts(ids, callback) {
	      this.nativeMessaging.connect();
	      this.nativeMessaging.postMessage({
	        "DeleteAccounts": [{
	          "ids": ids
	        }]
	      }, callback);
	    }
	  }, {
	    key: "getAccounts",
	    value: function getAccounts(callback) {
	      this.nativeMessaging.connect();
	      this.nativeMessaging.postMessage({
	        "GetAccounts": []
	      }, callback);
	    }
	  }, {
	    key: "isAccountExist",
	    value: function isAccountExist(uri, callback) {
	      this.nativeMessaging.connect();
	      this.nativeMessaging.postMessage({
	        "IsAccountExist": [{
	          "uri": uri
	        }]
	      }, callback);
	    }
	  }, {
	    key: "requestPassword",
	    value: function requestPassword(id, callback) {
	      this.nativeMessaging.connect();
	      this.nativeMessaging.postMessage({
	        "RequestPassword": [{
	          "id": id
	        }]
	      }, callback);
	    }
	  }, {
	    key: "inputMasterPassword",
	    value: function inputMasterPassword(callback) {
	      this.nativeMessaging.connect();
	      this.nativeMessaging.postMessage({
	        "InputMasterPassword": []
	      }, callback);
	    }
	  }]);
	
	  return Api;
	}();
	
	exports.default = Api;

/***/ },
/* 5 */
/***/ function(module, exports) {

	"use strict";
	
	//const CONNECTION_ATTEMPTS_MAX = 32;
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var NativeMessaging = function () {
	  function NativeMessaging(callbackOnConnectionFailure) {
	    _classCallCheck(this, NativeMessaging);
	
	    this.connectionAttempt = 0;
	    this.callback = null;
	    this.callbackOnConnectionFailure = callbackOnConnectionFailure;
	    this.hostName = "io.gnn.keymaker"; //TODO Pull from manifest
	    this.port = null;
	  }
	
	  _createClass(NativeMessaging, [{
	    key: "connect",
	    value: function connect() {
	      console.log('NativeMessaging.js: Connecting to backend host: ' + this.hostName);
	      this.port = chrome.runtime.connectNative(this.hostName);
	      this.port.onMessage.addListener(this.onMessage.bind(this));
	      this.port.onDisconnect.addListener(this.onDisconnected.bind(this));
	    }
	  }, {
	    key: "onDisconnected",
	    value: function onDisconnected() {
	      console.log('NativeMessaging.js: Disconnected: ' + chrome.runtime.lastError.message);
	      /*
	      this.connectionAttempt += 1;
	       if (this.connectionAttempt > CONNECTION_ATTEMPTS_MAX) {
	        this.port = null;
	        this.connectionAttempt = 0;
	        this.funcCallbackOnFail();
	      } else {
	        console.log('NativeMessaging.js: Retrying. Attempt ' + this.connectionAttempt + ' out of ' + CONNECTION_ATTEMPTS_MAX);
	        this.connect();
	        this.postMessage(this.message, this.callback);
	      }*/
	    }
	  }, {
	    key: "postMessage",
	    value: function postMessage(message, callback) {
	      this.message = message;
	      this.callback = callback;
	      console.log('NativeMessaging.js: Posting message: ' + JSON.stringify(this.message));
	      this.port.postMessage(message);
	    }
	  }, {
	    key: "onMessage",
	    value: function onMessage(message) {
	      //TODO Check message type and whether we need all this conversion
	      var strMessage = JSON.stringify(message);
	      console.log('NativeMessaging.js: Received message: ' + strMessage);
	
	      this.port = null;
	
	      var objMessage = jQuery.parseJSON(strMessage);
	      this.callback(objMessage);
	    }
	  }]);
	
	  return NativeMessaging;
	}();
	
	exports.default = NativeMessaging;

/***/ }
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgNzUzMTJlN2U5MjA2NTA3NGE3YTA/MmQ1MioiLCJ3ZWJwYWNrOi8vLy4vYXBwL3NjcmlwdHMvYXBpLmpzP2IzMDQqIiwid2VicGFjazovLy8uL2FwcC9zY3JpcHRzL25hdGl2ZV9tZXNzYWdpbmcuanM/NDhhYyoiXSwibmFtZXMiOlsiQXBpIiwib25GYWlsIiwibmF0aXZlTWVzc2FnaW5nIiwibmFtZSIsInVzZXIiLCJ1cmkiLCJwYXNzd29yZFR5cGUiLCJjb3VudGVyIiwiY2FsbGJhY2siLCJjb25uZWN0IiwicG9zdE1lc3NhZ2UiLCJOdW1iZXIiLCJpZCIsImlkcyIsIk5hdGl2ZU1lc3NhZ2luZyIsImNhbGxiYWNrT25Db25uZWN0aW9uRmFpbHVyZSIsImNvbm5lY3Rpb25BdHRlbXB0IiwiaG9zdE5hbWUiLCJwb3J0IiwiY29uc29sZSIsImxvZyIsImNocm9tZSIsInJ1bnRpbWUiLCJjb25uZWN0TmF0aXZlIiwib25NZXNzYWdlIiwiYWRkTGlzdGVuZXIiLCJiaW5kIiwib25EaXNjb25uZWN0Iiwib25EaXNjb25uZWN0ZWQiLCJsYXN0RXJyb3IiLCJtZXNzYWdlIiwiSlNPTiIsInN0cmluZ2lmeSIsInN0ck1lc3NhZ2UiLCJvYmpNZXNzYWdlIiwialF1ZXJ5IiwicGFyc2VKU09OIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsdUJBQWU7QUFDZjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0Q0E7Ozs7Ozs7O0FBRUE7Ozs7Ozs7O0tBRXFCQSxHO0FBQ25CLGdCQUFZQyxNQUFaLEVBQW9CO0FBQUE7O0FBQ2xCLFVBQUtDLGVBQUwsR0FBdUIsK0JBQW9CRCxNQUFwQixDQUF2QjtBQUNEOzs7O21DQUVhRSxJLEVBQU1DLEksRUFBTUMsRyxFQUFLQyxZLEVBQWNDLE8sRUFBU0MsUSxFQUFVO0FBQzlELFlBQUtOLGVBQUwsQ0FBcUJPLE9BQXJCO0FBQ0EsWUFBS1AsZUFBTCxDQUFxQlEsV0FBckIsQ0FBaUM7QUFDL0IsMEJBQWlCLENBQ2Y7QUFDRSxtQkFBUVAsSUFEVjtBQUVFLG1CQUFRQyxJQUZWO0FBR0Usa0JBQU9DLEdBSFQ7QUFJRSwyQkFBZ0JNLE9BQU9MLFlBQVAsQ0FKbEI7QUFLRSxzQkFBV0ssT0FBT0osT0FBUDtBQUxiLFVBRGU7QUFEYyxRQUFqQyxFQVVHQyxRQVZIO0FBV0Q7OzttQ0FFYUksRSxFQUFJVCxJLEVBQU1DLEksRUFBTUMsRyxFQUFLQyxZLEVBQWNDLE8sRUFBU0MsUSxFQUFVO0FBQ2xFLFlBQUtOLGVBQUwsQ0FBcUJPLE9BQXJCO0FBQ0EsWUFBS1AsZUFBTCxDQUFxQlEsV0FBckIsQ0FBaUM7QUFDL0IsMEJBQWlCLENBQ2Y7QUFDRSxpQkFBTUMsT0FBT0MsRUFBUCxDQURSO0FBRUUsbUJBQVFULElBRlY7QUFHRSxtQkFBUUMsSUFIVjtBQUlFLGtCQUFPQyxHQUpUO0FBS0UsMkJBQWdCTSxPQUFPTCxZQUFQLENBTGxCO0FBTUUsc0JBQVdLLE9BQU9KLE9BQVA7QUFOYixVQURlO0FBRGMsUUFBakMsRUFXR0MsUUFYSDtBQVlEOzs7bUNBRWFJLEUsRUFBSUosUSxFQUFVO0FBQzFCLFlBQUtOLGVBQUwsQ0FBcUJPLE9BQXJCO0FBQ0EsWUFBS1AsZUFBTCxDQUFxQlEsV0FBckIsQ0FBaUM7QUFDL0IsMEJBQWlCLENBQ2Y7QUFDRSxpQkFBTUMsT0FBT0MsRUFBUDtBQURSLFVBRGU7QUFEYyxRQUFqQyxFQU1HSixRQU5IO0FBT0Q7OztvQ0FFY0ssRyxFQUFLTCxRLEVBQVU7QUFDNUIsWUFBS04sZUFBTCxDQUFxQk8sT0FBckI7QUFDQSxZQUFLUCxlQUFMLENBQXFCUSxXQUFyQixDQUFpQztBQUMvQiwyQkFBa0IsQ0FDaEI7QUFDRSxrQkFBT0c7QUFEVCxVQURnQjtBQURhLFFBQWpDLEVBTUdMLFFBTkg7QUFPRDs7O2lDQUVXQSxRLEVBQVU7QUFDcEIsWUFBS04sZUFBTCxDQUFxQk8sT0FBckI7QUFDQSxZQUFLUCxlQUFMLENBQXFCUSxXQUFyQixDQUFpQztBQUMvQix3QkFBZTtBQURnQixRQUFqQyxFQUVHRixRQUZIO0FBR0Q7OztvQ0FFY0gsRyxFQUFLRyxRLEVBQVU7QUFDNUIsWUFBS04sZUFBTCxDQUFxQk8sT0FBckI7QUFDQSxZQUFLUCxlQUFMLENBQXFCUSxXQUFyQixDQUFpQztBQUMvQiwyQkFBa0IsQ0FDaEI7QUFDRSxrQkFBT0w7QUFEVCxVQURnQjtBQURhLFFBQWpDLEVBTUdHLFFBTkg7QUFPRDs7O3FDQUVlSSxFLEVBQUlKLFEsRUFBVTtBQUM1QixZQUFLTixlQUFMLENBQXFCTyxPQUFyQjtBQUNBLFlBQUtQLGVBQUwsQ0FBcUJRLFdBQXJCLENBQWlDO0FBQy9CLDRCQUFtQixDQUNqQjtBQUNFLGlCQUFNRTtBQURSLFVBRGlCO0FBRFksUUFBakMsRUFNR0osUUFOSDtBQU9EOzs7eUNBRW1CQSxRLEVBQVU7QUFDNUIsWUFBS04sZUFBTCxDQUFxQk8sT0FBckI7QUFDQSxZQUFLUCxlQUFMLENBQXFCUSxXQUFyQixDQUFpQztBQUMvQixnQ0FBdUI7QUFEUSxRQUFqQyxFQUVHRixRQUZIO0FBR0Q7Ozs7OzttQkE1RmtCUixHOzs7Ozs7QUNKckI7O0FBRUE7Ozs7Ozs7Ozs7S0FFcUJjLGU7QUFDbkIsNEJBQVlDLDJCQUFaLEVBQXlDO0FBQUE7O0FBQ3ZDLFVBQUtDLGlCQUFMLEdBQXlCLENBQXpCO0FBQ0EsVUFBS1IsUUFBTCxHQUFnQixJQUFoQjtBQUNBLFVBQUtPLDJCQUFMLEdBQW1DQSwyQkFBbkM7QUFDQSxVQUFLRSxRQUFMLEdBQWdCLGlCQUFoQixDQUp1QyxDQUlKO0FBQ25DLFVBQUtDLElBQUwsR0FBWSxJQUFaO0FBQ0Q7Ozs7K0JBRVM7QUFDUkMsZUFBUUMsR0FBUixDQUFZLHFEQUFxRCxLQUFLSCxRQUF0RTtBQUNBLFlBQUtDLElBQUwsR0FBWUcsT0FBT0MsT0FBUCxDQUFlQyxhQUFmLENBQTZCLEtBQUtOLFFBQWxDLENBQVo7QUFDQSxZQUFLQyxJQUFMLENBQVVNLFNBQVYsQ0FBb0JDLFdBQXBCLENBQWdDLEtBQUtELFNBQUwsQ0FBZUUsSUFBZixDQUFvQixJQUFwQixDQUFoQztBQUNBLFlBQUtSLElBQUwsQ0FBVVMsWUFBVixDQUF1QkYsV0FBdkIsQ0FBbUMsS0FBS0csY0FBTCxDQUFvQkYsSUFBcEIsQ0FBeUIsSUFBekIsQ0FBbkM7QUFDRDs7O3NDQUVnQjtBQUNmUCxlQUFRQyxHQUFSLENBQVksdUNBQXVDQyxPQUFPQyxPQUFQLENBQWVPLFNBQWYsQ0FBeUJDLE9BQTVFO0FBQ0E7Ozs7Ozs7Ozs7O0FBWUQ7OztpQ0FFV0EsTyxFQUFTdEIsUSxFQUFVO0FBQzdCLFlBQUtzQixPQUFMLEdBQWVBLE9BQWY7QUFDQSxZQUFLdEIsUUFBTCxHQUFnQkEsUUFBaEI7QUFDQVcsZUFBUUMsR0FBUixDQUFZLDBDQUEwQ1csS0FBS0MsU0FBTCxDQUFlLEtBQUtGLE9BQXBCLENBQXREO0FBQ0EsWUFBS1osSUFBTCxDQUFVUixXQUFWLENBQXNCb0IsT0FBdEI7QUFDRDs7OytCQUVTQSxPLEVBQVM7QUFDakI7QUFDQSxXQUFJRyxhQUFhRixLQUFLQyxTQUFMLENBQWVGLE9BQWYsQ0FBakI7QUFDQVgsZUFBUUMsR0FBUixDQUFZLDJDQUEyQ2EsVUFBdkQ7O0FBRUEsWUFBS2YsSUFBTCxHQUFZLElBQVo7O0FBRUEsV0FBSWdCLGFBQWFDLE9BQU9DLFNBQVAsQ0FBaUJILFVBQWpCLENBQWpCO0FBQ0EsWUFBS3pCLFFBQUwsQ0FBYzBCLFVBQWQ7QUFDRDs7Ozs7O21CQWhEa0JwQixlIiwiZmlsZSI6ImFwaS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKVxuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuXG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRleHBvcnRzOiB7fSxcbiBcdFx0XHRpZDogbW9kdWxlSWQsXG4gXHRcdFx0bG9hZGVkOiBmYWxzZVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sb2FkZWQgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKDApO1xuXG5cblxuLyoqIFdFQlBBQ0sgRk9PVEVSICoqXG4gKiogd2VicGFjay9ib290c3RyYXAgNzUzMTJlN2U5MjA2NTA3NGE3YTBcbiAqKi8iLCJcInVzZSBzdHJpY3RcIjtcblxuaW1wb3J0IE5hdGl2ZU1lc3NhZ2luZyBmcm9tICcuL25hdGl2ZV9tZXNzYWdpbmcuanMnO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBBcGkge1xuICBjb25zdHJ1Y3RvcihvbkZhaWwpIHtcbiAgICB0aGlzLm5hdGl2ZU1lc3NhZ2luZyA9IG5ldyBOYXRpdmVNZXNzYWdpbmcob25GYWlsKTtcbiAgfVxuXG4gIGFjY291bnRDcmVhdGUobmFtZSwgdXNlciwgdXJpLCBwYXNzd29yZFR5cGUsIGNvdW50ZXIsIGNhbGxiYWNrKSB7XG4gICAgdGhpcy5uYXRpdmVNZXNzYWdpbmcuY29ubmVjdCgpO1xuICAgIHRoaXMubmF0aXZlTWVzc2FnaW5nLnBvc3RNZXNzYWdlKHtcbiAgICAgIFwiQ3JlYXRlQWNjb3VudFwiOiBbXG4gICAgICAgIHtcbiAgICAgICAgICBcIm5hbWVcIjogbmFtZSxcbiAgICAgICAgICBcInVzZXJcIjogdXNlcixcbiAgICAgICAgICBcInVyaVwiOiB1cmksXG4gICAgICAgICAgXCJwYXNzd29yZFR5cGVcIjogTnVtYmVyKHBhc3N3b3JkVHlwZSksXG4gICAgICAgICAgXCJjb3VudGVyXCI6IE51bWJlcihjb3VudGVyKVxuICAgICAgICB9XG4gICAgICBdXG4gICAgfSwgY2FsbGJhY2spO1xuICB9XG5cbiAgdXBkYXRlQWNjb3VudChpZCwgbmFtZSwgdXNlciwgdXJpLCBwYXNzd29yZFR5cGUsIGNvdW50ZXIsIGNhbGxiYWNrKSB7XG4gICAgdGhpcy5uYXRpdmVNZXNzYWdpbmcuY29ubmVjdCgpO1xuICAgIHRoaXMubmF0aXZlTWVzc2FnaW5nLnBvc3RNZXNzYWdlKHtcbiAgICAgIFwiVXBkYXRlQWNjb3VudFwiOiBbXG4gICAgICAgIHtcbiAgICAgICAgICBcImlkXCI6IE51bWJlcihpZCksXG4gICAgICAgICAgXCJuYW1lXCI6IG5hbWUsXG4gICAgICAgICAgXCJ1c2VyXCI6IHVzZXIsXG4gICAgICAgICAgXCJ1cmlcIjogdXJpLFxuICAgICAgICAgIFwicGFzc3dvcmRUeXBlXCI6IE51bWJlcihwYXNzd29yZFR5cGUpLFxuICAgICAgICAgIFwiY291bnRlclwiOiBOdW1iZXIoY291bnRlcilcbiAgICAgICAgfVxuICAgICAgXVxuICAgIH0sIGNhbGxiYWNrKTtcbiAgfVxuXG4gIGRlbGV0ZUFjY291bnQoaWQsIGNhbGxiYWNrKSB7XG4gICAgdGhpcy5uYXRpdmVNZXNzYWdpbmcuY29ubmVjdCgpO1xuICAgIHRoaXMubmF0aXZlTWVzc2FnaW5nLnBvc3RNZXNzYWdlKHtcbiAgICAgIFwiRGVsZXRlQWNjb3VudFwiOiBbXG4gICAgICAgIHtcbiAgICAgICAgICBcImlkXCI6IE51bWJlcihpZClcbiAgICAgICAgfVxuICAgICAgXVxuICAgIH0sIGNhbGxiYWNrKTtcbiAgfVxuXG4gIGRlbGV0ZUFjY291bnRzKGlkcywgY2FsbGJhY2spIHtcbiAgICB0aGlzLm5hdGl2ZU1lc3NhZ2luZy5jb25uZWN0KCk7XG4gICAgdGhpcy5uYXRpdmVNZXNzYWdpbmcucG9zdE1lc3NhZ2Uoe1xuICAgICAgXCJEZWxldGVBY2NvdW50c1wiOiBbXG4gICAgICAgIHtcbiAgICAgICAgICBcImlkc1wiOiBpZHNcbiAgICAgICAgfVxuICAgICAgXVxuICAgIH0sIGNhbGxiYWNrKTtcbiAgfVxuXG4gIGdldEFjY291bnRzKGNhbGxiYWNrKSB7XG4gICAgdGhpcy5uYXRpdmVNZXNzYWdpbmcuY29ubmVjdCgpO1xuICAgIHRoaXMubmF0aXZlTWVzc2FnaW5nLnBvc3RNZXNzYWdlKHtcbiAgICAgIFwiR2V0QWNjb3VudHNcIjogW11cbiAgICB9LCBjYWxsYmFjayk7XG4gIH1cblxuICBpc0FjY291bnRFeGlzdCh1cmksIGNhbGxiYWNrKSB7XG4gICAgdGhpcy5uYXRpdmVNZXNzYWdpbmcuY29ubmVjdCgpO1xuICAgIHRoaXMubmF0aXZlTWVzc2FnaW5nLnBvc3RNZXNzYWdlKHtcbiAgICAgIFwiSXNBY2NvdW50RXhpc3RcIjogW1xuICAgICAgICB7XG4gICAgICAgICAgXCJ1cmlcIjogdXJpXG4gICAgICAgIH1cbiAgICAgIF1cbiAgICB9LCBjYWxsYmFjayk7XG4gIH1cblxuICByZXF1ZXN0UGFzc3dvcmQoaWQsIGNhbGxiYWNrKSB7XG4gICAgdGhpcy5uYXRpdmVNZXNzYWdpbmcuY29ubmVjdCgpO1xuICAgIHRoaXMubmF0aXZlTWVzc2FnaW5nLnBvc3RNZXNzYWdlKHtcbiAgICAgIFwiUmVxdWVzdFBhc3N3b3JkXCI6IFtcbiAgICAgICAge1xuICAgICAgICAgIFwiaWRcIjogaWRcbiAgICAgICAgfVxuICAgICAgXVxuICAgIH0sIGNhbGxiYWNrKTtcbiAgfVxuXG4gIGlucHV0TWFzdGVyUGFzc3dvcmQoY2FsbGJhY2spIHtcbiAgICB0aGlzLm5hdGl2ZU1lc3NhZ2luZy5jb25uZWN0KCk7XG4gICAgdGhpcy5uYXRpdmVNZXNzYWdpbmcucG9zdE1lc3NhZ2Uoe1xuICAgICAgXCJJbnB1dE1hc3RlclBhc3N3b3JkXCI6IFtdXG4gICAgfSwgY2FsbGJhY2spO1xuICB9XG59XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2FwcC9zY3JpcHRzL2FwaS5qc1xuICoqLyIsIlwidXNlIHN0cmljdFwiO1xuXG4vL2NvbnN0IENPTk5FQ1RJT05fQVRURU1QVFNfTUFYID0gMzI7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE5hdGl2ZU1lc3NhZ2luZyB7XG4gIGNvbnN0cnVjdG9yKGNhbGxiYWNrT25Db25uZWN0aW9uRmFpbHVyZSkge1xuICAgIHRoaXMuY29ubmVjdGlvbkF0dGVtcHQgPSAwO1xuICAgIHRoaXMuY2FsbGJhY2sgPSBudWxsO1xuICAgIHRoaXMuY2FsbGJhY2tPbkNvbm5lY3Rpb25GYWlsdXJlID0gY2FsbGJhY2tPbkNvbm5lY3Rpb25GYWlsdXJlXG4gICAgdGhpcy5ob3N0TmFtZSA9IFwiaW8uZ25uLmtleW1ha2VyXCI7IC8vVE9ETyBQdWxsIGZyb20gbWFuaWZlc3RcbiAgICB0aGlzLnBvcnQgPSBudWxsO1xuICB9XG5cbiAgY29ubmVjdCgpIHtcbiAgICBjb25zb2xlLmxvZygnTmF0aXZlTWVzc2FnaW5nLmpzOiBDb25uZWN0aW5nIHRvIGJhY2tlbmQgaG9zdDogJyArIHRoaXMuaG9zdE5hbWUpO1xuICAgIHRoaXMucG9ydCA9IGNocm9tZS5ydW50aW1lLmNvbm5lY3ROYXRpdmUodGhpcy5ob3N0TmFtZSk7XG4gICAgdGhpcy5wb3J0Lm9uTWVzc2FnZS5hZGRMaXN0ZW5lcih0aGlzLm9uTWVzc2FnZS5iaW5kKHRoaXMpKTtcbiAgICB0aGlzLnBvcnQub25EaXNjb25uZWN0LmFkZExpc3RlbmVyKHRoaXMub25EaXNjb25uZWN0ZWQuYmluZCh0aGlzKSk7XG4gIH1cblxuICBvbkRpc2Nvbm5lY3RlZCgpIHtcbiAgICBjb25zb2xlLmxvZygnTmF0aXZlTWVzc2FnaW5nLmpzOiBEaXNjb25uZWN0ZWQ6ICcgKyBjaHJvbWUucnVudGltZS5sYXN0RXJyb3IubWVzc2FnZSk7XG4gICAgLypcbiAgICB0aGlzLmNvbm5lY3Rpb25BdHRlbXB0ICs9IDE7XG5cbiAgICBpZiAodGhpcy5jb25uZWN0aW9uQXR0ZW1wdCA+IENPTk5FQ1RJT05fQVRURU1QVFNfTUFYKSB7XG4gICAgICB0aGlzLnBvcnQgPSBudWxsO1xuICAgICAgdGhpcy5jb25uZWN0aW9uQXR0ZW1wdCA9IDA7XG4gICAgICB0aGlzLmZ1bmNDYWxsYmFja09uRmFpbCgpO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb25zb2xlLmxvZygnTmF0aXZlTWVzc2FnaW5nLmpzOiBSZXRyeWluZy4gQXR0ZW1wdCAnICsgdGhpcy5jb25uZWN0aW9uQXR0ZW1wdCArICcgb3V0IG9mICcgKyBDT05ORUNUSU9OX0FUVEVNUFRTX01BWCk7XG4gICAgICB0aGlzLmNvbm5lY3QoKTtcbiAgICAgIHRoaXMucG9zdE1lc3NhZ2UodGhpcy5tZXNzYWdlLCB0aGlzLmNhbGxiYWNrKTtcbiAgICB9Ki9cbiAgfVxuXG4gIHBvc3RNZXNzYWdlKG1lc3NhZ2UsIGNhbGxiYWNrKSB7XG4gICAgdGhpcy5tZXNzYWdlID0gbWVzc2FnZTtcbiAgICB0aGlzLmNhbGxiYWNrID0gY2FsbGJhY2s7XG4gICAgY29uc29sZS5sb2coJ05hdGl2ZU1lc3NhZ2luZy5qczogUG9zdGluZyBtZXNzYWdlOiAnICsgSlNPTi5zdHJpbmdpZnkodGhpcy5tZXNzYWdlKSk7XG4gICAgdGhpcy5wb3J0LnBvc3RNZXNzYWdlKG1lc3NhZ2UpO1xuICB9XG5cbiAgb25NZXNzYWdlKG1lc3NhZ2UpIHtcbiAgICAvL1RPRE8gQ2hlY2sgbWVzc2FnZSB0eXBlIGFuZCB3aGV0aGVyIHdlIG5lZWQgYWxsIHRoaXMgY29udmVyc2lvblxuICAgIGxldCBzdHJNZXNzYWdlID0gSlNPTi5zdHJpbmdpZnkobWVzc2FnZSk7XG4gICAgY29uc29sZS5sb2coJ05hdGl2ZU1lc3NhZ2luZy5qczogUmVjZWl2ZWQgbWVzc2FnZTogJyArIHN0ck1lc3NhZ2UpO1xuXG4gICAgdGhpcy5wb3J0ID0gbnVsbDtcblxuICAgIGxldCBvYmpNZXNzYWdlID0galF1ZXJ5LnBhcnNlSlNPTihzdHJNZXNzYWdlKTtcbiAgICB0aGlzLmNhbGxiYWNrKG9iak1lc3NhZ2UpO1xuICB9XG59XG5cblxuXG4vKiogV0VCUEFDSyBGT09URVIgKipcbiAqKiAuL2FwcC9zY3JpcHRzL25hdGl2ZV9tZXNzYWdpbmcuanNcbiAqKi8iXSwic291cmNlUm9vdCI6IiJ9